class CommentsController < ApplicationController
  before_filter :find_topic

  def create
    @comment = @topic.comments.build(params[:comment])
    @comment.user = current_user
    if @comment.save
      redirect_to @topic, notice: 'Comment was successfully created.'
    else
      redirect_to @topic, alert: 'Fail to save comment'
    end
  end

  def destroy
    @comment = @topic.comments.find(params[:id])
    authorize! :destroy, @comment
    if @comment.destroy
      redirect_to @topic, notice: 'Comment was successfully destroyed.'
    end
  end

  protected
  def find_topic
    @topic = Topic.find(params[:topic_id])
  end
end
