class TopicsController < InheritedResources::Base
  actions :all, :except => [:edit, :update]

  before_filter :authenticate_user!, :except => [:show, :index]
  before_filter :find_node, :only => [:create]
  before_filter :find_node_by_key, :only => [:new]

  def new
    if @node
      @topic = @node.topics.new
    else
      redirect_to root_url, alert: 'No such node.'
    end
  end

  def show
    @topic = Topic.find(params[:id])
    @comments = @topic.comments
    @comment = @topic.comments.new
  end

  def create
    @topic = current_user.topics.build(params[:topic])
    @topic.node = @node
    if @topic.save
      redirect_to @topic, notice: 'Topic was successfully created.'
    else
      render action: 'new'
    end
  end

  def destroy
    @topic = Topic.find(params[:id])
    authorize! :destroy, @topic
    if @topic.destroy
      redirect_to topics_url, notice: 'Topic was successfully destroyed.'
    end
  end

  protected
  def find_node_by_key
    @node = Node.find_by_key(params[:key])
  end

  def find_node
    @node = Node.find(params[:node_id])
  end
end
