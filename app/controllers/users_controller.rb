class UsersController < ApplicationController
  def show
    if @user = User.find_by_username(params[:username])
      @topics = @user.topics
      @comments = @user.comments
    else
      redirect_to root_url, notice: 'No such user.'
    end
  end
end
