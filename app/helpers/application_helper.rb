module ApplicationHelper

  def user_avatar_width_for_size(size)
    case size
    when :normal then 48
    when :small then 16
    when :large then 64
    when :big then 120
    else size
    end
  end

  def user_avatar_tag(user, size = :normal, opts = {})
    width = user_avatar_width_for_size(size)
    avatar_img = "https://secure.gravatar.com/avatar/#{user.email_md5}?s=#{width}"
    image_tag(avatar_img, alt: user.username, class: 'avatar')
  end
end
