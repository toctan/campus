class Comment < ActiveRecord::Base
  attr_accessible :content

  belongs_to :user
  belongs_to :topic

  validates :content, :user_id, :topic_id, :presence => true

  after_create :update_parent_topic

  def update_parent_topic
    topic.update_last_comment(self)
  end
end
