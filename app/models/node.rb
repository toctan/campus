class Node < ActiveRecord::Base
  attr_accessible :name, :key, :description, :avatar

  has_many :topics

  validates :name, :key, :presence => true
  validates :key, :uniqueness => { case_sensitive: false }
  validates :key, :format => { with: /^[a-z0-9]+$/ }

  def update_topics_count
    self.topics_count += 1
    self.save
  end
end
