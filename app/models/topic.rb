class Topic < ActiveRecord::Base
  attr_accessible :title, :content

  belongs_to :user
  belongs_to :node

  has_many :comments

  validates :title, :user_id, :node_id, :presence => true

  after_create :update_parent_node

  def update_parent_node
    node.update_topics_count
  end

  def update_last_comment(comment)
    self.last_replied_at = comment.created_at
    self.last_replied_by = comment.user.username
    self.comments_count += 1
    self.save
  end
end
