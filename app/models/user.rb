class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :username, :email, :password, :remember_me

  has_many :topics, :dependent => :destroy
  has_many :comments, :dependent => :destroy

  validates :username, :presence => true
  validates :username, :uniqueness => { case_sensitive: false }
  validates :username, :length => { :maximum => 12 }
  validates :username, :format => { with: /^[a-z0-9]+$/ }

  def email=(val)
    self.email_md5 = Digest::MD5.hexdigest(val || "")
    self[:email] = val
  end
end
