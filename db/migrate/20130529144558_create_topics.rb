class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string   :title
      t.text     :content
      t.integer  :user_id
      t.integer  :node_id
      t.integer  :comments_count,  :default => 0, :null => false
      t.string   :last_replied_by, :default => ""
      t.datetime :last_replied_at

      t.timestamps
    end

    add_index :topics, :node_id
    add_index :topics, :user_id
  end
end
