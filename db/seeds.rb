# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

p 'Creating seeds data for development ...'

p ' > users ...'

FactoryGirl.create(:admin)

p ' > nodes ...'

FactoryGirl.create(:node)

p ' > topics ...'


p 'Finished creating seeds data for development.'
