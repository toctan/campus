Feature: browse comments
  In order to discuss with other users
  A user will need to browse comments

  Scenario: a user read a topic
    When I read a topic
    Then I should see the comments
