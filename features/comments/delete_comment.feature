Feature: Delete comment
  In order to keep the forum clean
  A admin will need to delete inapproriate comments

  Scenario: admin deletes a commen
    Given I am an admin
    When I read a topic
    Then I should see the delete comment button
    When I click the delete comment button
    Then I should see the comment get deleted
    

  Scenario: normal user tries to delete a comment
    Given I am a signed in user
    When I read a topic
    Then I should not see the delete comment button
    



  



    

  
