Feature: new comment
  In order to reply to a topic
  A user will need to post new comments

  Scenario: a signed in user post a new comment
    Given I am a signed in user
    When I read a topic
    Then I should be able to post a new comment
    When I submit a new comment
    Then I should see a successful message

  Scenario: a not signed in user read a topic
    Given I am not signed in
    When I read a topic
    Then I should not be able to post a new comment
    And I should see a sign in and sign up link
