Before do
  @node = FactoryGirl.create(:node)
  @topic = FactoryGirl.create(:topic, node: @node)
  @comment = FactoryGirl.create(:comment, topic: @topic)
end

When(/^I read a topic$/) do
  visit topic_path(@topic)
end

Then(/^I should be able to post a new comment$/) do
  page.should have_selector('input#comment_content')
end

When(/^I submit a new comment$/) do
  fill_in 'comment_content', :with => "first comment"
  click_button('Create Comment')
end

Then(/^I should see a successful message$/) do
  page.should have_selector('.alert-success')
end

Then(/^I should not be able to post a new comment$/) do
  page.should_not have_selector('#comment_content')
end

Then(/^I should see a sign in and sign up link$/) do
  page.should have_link('Login')
end

Then(/^I should see the comments$/) do
  page.should have_selector('.comments_index')
end

Then(/^I should see the delete comment button$/) do
  find('.comments_index').should have_link('Delete Comment')
end

When(/^I click the delete comment button$/) do
  click_link('Delete Comment')
end

Then(/^I should see the comment get deleted$/) do
  find('.alert').should have_content('Comment was successfully destroyed.')
end

Then(/^I should not see the delete comment button$/) do
  find('.comments_index').should_not have_link('Delete Comment')
end
