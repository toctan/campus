Given(/^I am a signed in user$/) do
  create_user
  sign_in
end

When(/^I visit the new topic page$/) do
  visit "/new/#{@node.key}"
end

Then(/^I should be able to edit the new topic$/) do
  page.should have_selector('input#topic_title')
  page.should have_selector('textarea#topic_content')
end

When(/^I finish the edit and submmit$/) do
  @topic ||= { title: 'good morning', content: 'hello'}
  fill_in 'Title', :with => @topic.title
  fill_in 'Content', :with => @topic.content
  click_button('Create Topic')
end

Then(/^I should see the new topic get created$/) do
  find('.alert-success').should have_content('Topic was successfully created.')
  visit '/topics'
  find('#topics_index').should have_link(@topic.title)
end

Then(/^I should see a need to sign in message$/) do
  find('.alert').should have_content('need to sign in')
end

Then(/^I should see the sign in form$/) do
  find('h2').should have_content('Sign in')
end

Given(/^I am an admin$/) do
  create_admin
  sign_in
end

Then(/^I should be able to delete the topic$/) do
  visit topic_path(@topic)
end

When(/^I click the delete button$/) do
  click_link 'Delete'
end

Then(/^I should see the topic get deleted$/) do
  find('.alert-success').should have_content('Topic was successfully destroyed.')
end

Then(/^I should not see the delete button$/) do
  page.should_not have_link('Delete')
end
