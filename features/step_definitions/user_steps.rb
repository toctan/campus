def create_visitor
  @visitor ||= { :username => "visitor", :email => "example@example.com",
    :password => "changeme" }
end

def find_user
  @user ||= User.where(:email => @visitor[:email]).first
end

def create_user
  create_visitor
  delete_user
  @user = FactoryGirl.create(:user, @visitor)
end

def create_admin
  create_visitor
  delete_user
  @user = FactoryGirl.create(:admin, @visitor)
end

def delete_user
  @user = User.where(username: @visitor[:username]).first
  @user.destroy unless @user.nil?
end

def sign_in
  visit new_user_session_path
  fill_in "user_username", :with => @visitor[:username]
  fill_in "user_password", :with => @visitor[:password]
  click_button "Sign in"
end

def sign_up
  delete_user
  visit new_user_registration_path
  fill_in "user_username", :with => @visitor[:username]
  fill_in "user_email", :with => @visitor[:email]
  fill_in "user_password", :with => @visitor[:password]
  click_button "Sign up"
  find_user
end

Given(/^I am a signed up user$/) do
  create_user
end

Given(/^I am not signed in$/) do
  visit destroy_user_session_path
end

Given(/^I am signed in$/) do
  create_user
  sign_in
end

When(/^I sign in with valid credentials$/) do
  create_visitor
  sign_in
end

When(/^I sign in with a wrong username$/) do
  create_visitor
  @visitor[:username] = "foobar"
  sign_in
end

When(/^I sign in with a wrong password$/) do
  create_visitor
  @visitor[:password] = "foobazfs"
  sign_in
end

When(/^I return to the site$/) do
  visit root_path
end

When(/^I sign up with valid user data$/) do
  create_visitor
  sign_up
end

When(/^I sign up with invalid email$/) do
  create_visitor
  @visitor = @visitor.merge(:email => "")
  sign_up
end

When(/^I sign up with invalid username$/) do
  create_visitor
  @visitor = @visitor.merge(:username => "@434")
  sign_up
end

When(/^I sign up with invalid password$/) do
  create_visitor
  @visitor = @visitor.merge(:password => "")
  sign_up
end

When(/^I click the confirmation link$/) do
  find_user
  visit('/users/confirmation?confirmation_token=' + @user.confirmation_token)
end

When(/^I sign out$/) do
  visit destroy_user_session_path
end

Then(/^I should be signed in$/) do
  page.should have_link "Logout"
  page.should_not have_link "Sign up"
  page.should_not have_link "Login"
end

Then(/^I see a successful sign in message$/) do
  find('.alert-success').should have_content("Signed in successfully.")
end

Then(/^I see an invalid login message$/) do
  find('.alert-error').should have_content 'Invalid'
end

Then(/^I should not be signed in$/) do
  page.should have_link "Sign up"
  page.should have_link "Login"
  page.should_not have_link "Logout"
end

Then(/^I should see a successful sign up message$/) do
  find('.alert-success').should have_content("successful")
end

Then(/^I should see an invalid message$/) do
  page.should have_selector('.help-inline')
end

Then(/^I should receive a confirmation email$/) do
  find('.alert-success').should have_content("sent to your email")
end

Then(/^I should see a signed out message$/) do
  find('.alert-success').should have_content "Signed out successfully."
end

When(/^I visit a user's profile page$/) do
  username = User.all.sample.username
  visit "/member/#{username}"
end

Then(/^I should see his avatar$/) do
  page.should have_selector('img.avatar')
end

Then(/^I should see his recent topics and comments$/) do
  page.should have_selector('.topics_index')
  page.should have_selector('.comments_index')
end
