Feature: Delete topic
  In order to keep the forum clean
  Admin will need to delete inappropriate topic

  Scenario: admin deletes a topic
    Given I am an admin
    When I read a topic
    Then I should be able to delete the topic
    When I click the delete button
    Then I should see the topic get deleted

  Scenario: normal user try to delete a topic
    Given I am a signed in user
    When I read a topic
    Then I should not see the delete button
    



  





  
