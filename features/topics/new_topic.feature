Feature: New topic
  In order to start a discussion,
  a user will need to post a new topic

  Scenario: post a new topic
    Given I am a signed in user
    When I visit the new topic page
    Then I should be able to edit the new topic
    When I finish the edit and submmit
    Then I should see the new topic get created

  Scenario: post a new topic before signed in
    Given I am not signed in
    When I visit the new topic page
    Then I should see a need to sign in message
    And I should see the sign in form
