Feature: Sign in
  In order to post a topic or access protected sections
  A user will need sign in
  
  Background:
    Given I am a signed up user
    And I am not signed in
  
  Scenario: User signs in successfully
    When I sign in with valid credentials
    Then I see a successful sign in message
    When I return to the site
    Then I should be signed in

  Scenario: User enters wrong username
    When I sign in with a wrong username
    Then I see an invalid login message
    And I should not be signed in
      
  Scenario: User enters wrong password
    When I sign in with a wrong password
    Then I see an invalid login message
    And I should not be signed in

    
    
    
    
  
      
    
    
    
    
    
  

