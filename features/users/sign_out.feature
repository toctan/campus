Feature: Sign out
  In order to protect my account from unauthorized access
  A signed user will need to sign out

  Scenario: User signs out
    Given I am signed in
    When I sign out
    Then I should see a signed out message
    When I return to the site
    Then I should not be signed in
    
    
    
    
    
    
    
  
