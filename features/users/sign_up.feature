Feature: Sign up
  In order to have a account
  A visitor will need sign up

  Background:
    Given I am not signed in
  
  Scenario: Visitor signs up successfully
    When I sign up with valid user data
    Then I should receive a confirmation email
    When I click the confirmation link
    Then I should see a successful sign up message
    And I should be signed in

  Scenario: Visitor signs up with invalid username
    When I sign up with invalid username
    Then I should see an invalid message
    
  Scenario: Visitor signs up with invalid email
    When I sign up with invalid email
    Then I should see an invalid message

  Scenario: Visitor signs up with invalid password
    When I sign up with invalid password
    Then I should see an invalid message
    
    
  
    
      
    
    
    
    
    
    
    
  
