FactoryGirl.define do
  factory :comment do
    content "MyString"

    user
    topic
  end
end
