FactoryGirl.define do
  factory :user do
    sequence(:username) { |n| "example#{n}" }
    sequence(:email) { |n| "example#{n}@example.com" }
    password "foobarbaz"
    confirmed_at Time.now
  end

  factory :admin, class: User do
    username     "toctan"
    email        "toctan@example.com"
    password     "foobarbaz"
    confirmed_at Time.now
    admin        true
  end
end
