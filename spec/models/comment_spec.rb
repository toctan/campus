require 'spec_helper'

describe Comment do
  it { should belong_to(:user) }
  it { should belong_to(:topic) }

  it { should validate_presence_of :content }
  it { should validate_presence_of :user_id }
  it { should validate_presence_of :topic_id }

  it { should allow_mass_assignment_of(:content) }
  it { should_not allow_mass_assignment_of(:user_id) }
  it { should_not allow_mass_assignment_of(:topic_id) }
end
