require 'spec_helper'

describe Node do
  let(:node) { FactoryGirl.create(:node) }

  it { should have_many(:topics) }

  it { should validate_presence_of :key }
  it { should validate_presence_of :name }

  it { should validate_uniqueness_of :key }

  it "should get updated upon topic creation" do
    count = node.topics_count
    FactoryGirl.create(:topic, node: node)
    Node.find(node.id).topics_count.should == count + 1
  end
end
