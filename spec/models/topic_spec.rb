require 'spec_helper'

describe Topic do
  let(:user) { FactoryGirl.create(:user) }
  let(:topic) { FactoryGirl.create(:topic) }

  it { should belong_to(:user) }
  it { should belong_to(:node) }
  it { should have_many(:comments) }

  it { should validate_presence_of :title }
  it { should validate_presence_of :user_id }
  it { should validate_presence_of :node_id }
  it { should_not validate_presence_of :content }

  it { should allow_mass_assignment_of(:title) }
  it { should allow_mass_assignment_of(:content) }
  it { should_not allow_mass_assignment_of(:user_id) }
  it { should_not allow_mass_assignment_of(:node_id) }

  it "should get updated after reply" do
    count = topic.comments_count
    comment = FactoryGirl.create(:comment, topic: topic, user: user)
    topic = Topic.find(topic.id)
    topic.last_replied_at.to_i.should == comment.created_at.to_i
    topic.last_replied_by.should == comment.user.username
    topic.comments_count.should == count + 1
  end
end
