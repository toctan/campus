require 'spec_helper'
require 'digest/md5'

describe User do
  it { should have_many(:topics) }
  it { should have_many(:comments) }

  it { should validate_presence_of :username }
  it { should validate_presence_of :password }
  it { should validate_presence_of :email }

  # it { should validate_uniqueness_of(:username) }
  it { should validate_uniqueness_of(:email) }

  it { should allow_mass_assignment_of(:username) }
  it { should allow_mass_assignment_of(:email) }
  it { should allow_mass_assignment_of(:password) }


  let(:user) do
    User.new(username: "foo",
             email: "foo@bar.com",
             password: "foobaz")
  end

  describe 'username' do
    it "should only contain a-z or 0-9" do
      user.should be_valid
      bad_names = ['g.6', 'f_5', 'be@fs', 'test.']
      bad_names.each do |name|
        user.username = name
        user.should_not be_valid
      end
    end

    it "should contain at most 12 characters" do
      user.username = 'a' * 13
      user.should_not be_valid
    end
  end

  describe 'email validation' do
    it "should be invalid" do
      emails = %w[user@foo,com user_at_foo.org example.user@foo.
                     foo@bar_baz.com foo@bar+baz.com]
      emails.each do |invalid_email|
        user.email = invalid_email
        user.should_not be_valid
      end
    end
  end

  describe "when email format is valid" do
    it "should be valid" do
      emails = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
      emails.each do |valid_email|
        user.email = valid_email
        user.should be_valid
      end
    end
  end

  describe "email_md5" do
    it "should get updated upon change on email" do
      user = User.first || FactoryGirl.create(:user)
      email = 'test@example.org'
      user.update_attribute(:email, email)
      user.confirm!
      user.email_md5.should == Digest::MD5.hexdigest(email)
    end
  end

  describe 'password' do
    it "should contain at least 6 characters" do
      user.password = 'a' * 5
      user.should_not be_valid
    end
  end
end
